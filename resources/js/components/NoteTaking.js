import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteNote, editNote, filterStatus, loadNotes } from './redux/action/noteAction';

class NoteTaking extends React.Component{
    constructor(props) {
        super(props)
    
        this.state = {
            notes:[],
            errors:[],
            save: false,
            not_save: false,
            search:""
        }
    }

    editNote = (id) => {
        this.props.editNote(id);
    }

    deleteNote = (id) => {
        this.props.deleteNote(id);
        // this.props.loadNotes();
    }

    changeSave = (e) => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            const { save, not_save } = this.state;
            this.props.filterStatus(save, not_save);
        });
    }

    /* searchHandler = (e) =>{
        this.setState({ [e.target.name]:e.target.value}, ()=>this.props.searchTitle(this.state.search));
    } */

    /* static getDerivedStateFromProps(nextProps, nextState) {
        if (nextProps.notes){
                nextState.notes = nextProps.notes
        }
        if (nextProps.errors) {
            nextState.errors = nextProps.errors
        }
        return null;
    } */

    componentWillReceiveProps(nextProps){
        if (nextProps.notes.length > 0) {
            this.setState({ notes: nextProps.notes });
        }
        if(nextProps.errors){
            this.setState({ errors: nextProps.errors});
        }
    }

    componentDidMount() {
        this.props.loadNotes();
    }

    
    render = () =>{
        const { notes, errors, search } = this.state;
        return(
            <div className="container">
                {/* Create Button */}
                <div className="form-group form-inline">
                    <Link to="/create" className="btn btn-outline-primary mt-2 mr-3">Create Note</Link>
                    <div className="form-group form-inline">
                        <div className="form-group form-check mr-3">
                            <input type="checkbox" className="form-check-input" name="save" id="save" 
                            onChange={(e)=>{
                                this.changeSave({
                                    target:{
                                        name: e.target.name,
                                        value: e.target.checked
                                    }
                                })
                            }} />
                            <label className="form-check-label" htmlFor="save">Save</label>
                        </div>
                        <div className="form-group form-check mr-3">
                            <input type="checkbox" className="form-check-input" name="not_save" id="not_save" 
                                onChange={(e) => {
                                    this.changeSave({
                                        target: {
                                            name: e.target.name,
                                            value: e.target.checked
                                        }
                                    })
                                }} />
                            <label className="form-check-label" htmlFor="not_save">Not Save</label>
                        </div>
                        {/* <input type="text" name="search" value={search} onChange={this.searchHandler} className="form-control" placeholder="Search Title" /> */}
                    </div>
                </div>
                {/* List of Notes */}
                <table className="table table-hover table-stripe mt-2">
                    <thead className="thead-dark">
                        <tr className="text-center">
                            <th><h4>Title</h4></th>
                            <th><h4>Text</h4></th>
                            <th><h4>Status</h4></th>
                            <th><h4>Action</h4></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            notes.length > 0 ? notes.map(note => {
                                return (
                                    <tr key={note.id} className="text-center">
                                        <td>{note.title}</td>
                                        <td>{note.text}</td>
                                        <td>{note.status == 0 ? "Unsave":"Save"}</td>
                                        <td>
                                            <Link to="/edit" className="btn btn-primary mr-2" onClick={()=>{this.editNote(note.id)}}>Edit</Link>
                                            <button type="button" className="btn btn-danger" onClick={() => { this.deleteNote(note.id) }}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            }) : <tr className="text-center"><td colSpan="8"><h4 className="text-danger">{errors}</h4></td></tr>
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        notes : state.notes,
        errors : state.errors
    }
}

export default connect(mapStateToProps, { loadNotes, editNote, deleteNote, filterStatus })(NoteTaking);
