import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import errorReducer from './reducer/errorReducer';
import noteReducer from './reducer/noteReducer';

const middleware = compose(applyMiddleware(thunk, logger));
const reducers = combineReducers({
    notes: noteReducer,
    errors: errorReducer
});
const store = createStore(reducers, middleware);

export default store;