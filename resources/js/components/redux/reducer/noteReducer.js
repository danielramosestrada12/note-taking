import { EDIT_NOTE, FILTER_NOTES, GET_NOTES } from '../types';

const initState = {};

const noteReducer = (state=initState, action) => {
    switch (action.type) {
        case GET_NOTES:
            return action.payload;
        case EDIT_NOTE:
            return action.payload;
        case FILTER_NOTES:
            return action.payload;
        default:
            return state;
    }
}

export default noteReducer;