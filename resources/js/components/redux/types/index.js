export const ADD_NOTE = 'ADD_NOTE';
export const GET_NOTES = 'GET_NOTES';
export const EDIT_NOTE = 'EDIT_NOTE';
export const FILTER_NOTES = 'FILTER_NOTES';

export const GET_ERRORS = 'GET_ERRORS';