import axios from 'axios';
import { EDIT_NOTE, FILTER_NOTES, GET_ERRORS, GET_NOTES } from '../types';

export const addNotes = (newNote, history) => dispatch =>{
    // HTTP Request to create new note
    axios.post('/api/note_taking', newNote, { headers: { 'Content-Type': 'application/json' } })
        .then(response => {
            // Destructure the response object
            const { status, message } = response.data;
            if (status === "success") {
                // Alert a message success
                alert(message);
                dispatch({
                    type: GET_ERRORS,
                    payload: {}
                })
                history.push('/');
            }
        })
        .catch(error => dispatch({
            type: GET_ERRORS,
            payload: error.response.data.errors
        }))
}

export const loadNotes = () => dispatch => {
    axios.get('/api/get_notes', { headers: { 'Content-Type': 'application/json' } })
        .then(response => {
            // Destructure the response object
            const { status, notes } = response.data;
            if (status === "success") {
                dispatch(getNotes(notes));
            }else{
                dispatch({
                    type: GET_ERRORS,
                    payload: response.data.message
                })
            }
        })
        .catch(error => dispatch({
            type:GET_ERRORS,
            payload: error.response.data
        }))
}

export const editNote = (id) => dispatch => {
    axios.get(`/api/note_taking/${id}/edit`, { headers: { 'Content-Type': 'application/json' } })
        .then(response => {
            // Destructure the response object
            const { status, note } = response.data;
            if (status === "success") {
                dispatch(setNote(note));
            }
        })
        .catch(error => dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        }))
}

export const updateNote = (newNote, history) => dispatch =>{
    const { id, title, text, status } = newNote;
    // HTTP Request to update new note
    axios.put(`/api/note_taking/${id}`, {title, text, status}, { headers: { 'Content-Type': 'application/json' } })
        .then(response => {
            // Destructure the response object
            const { status, message } = response.data;
            if (status === "success") {
                // Alert a message success
                alert(message);
                dispatch({
                    type: GET_ERRORS,
                    payload: {}
                })
                history.push('/');
            }
        })
        .catch(error => dispatch({
            type: GET_ERRORS,
            payload: error.response.data.errors
        }))
}

export const deleteNote = (id) => dispatch => {
    if(confirm('Are you sure you want to delete?')){
        // HTTP Request to delete new note
        axios.delete(`/api/note_taking/${id}`, { headers: { 'Content-Type': 'application/json' } })
            .then(response => {
                // Destructure the response object
                const { status, message } = response.data;
                if (status === "success") {
                    // Alert a message success
                    alert(message);
                    dispatch(loadNotes());
                }
            })
            .catch(error => dispatch({
                type: GET_ERRORS,
                payload: error.response.data
            }))
    }
}

export const filterStatus = (save, not_save) => dispatch =>{
    if(save && not_save){
        console.log('Status Both');
        dispatch(loadNotes());
    }else if(save && !not_save){
        console.log('Status Save Only');
        dispatch(filterSaveStatus());
    } else if(!save && not_save) {
        console.log('Status Not Save Only');
        dispatch(filterNotSaveStatus());
    }else{
        dispatch(loadNotes());
    }
}

export const filterSaveStatus = () => dispatch => {
    axios.get(`/api/save_status`, { headers: { 'Content-Type': 'application/json' } })
        .then(response => {
            console.log('filterSaveStatus', response.data);
            const { status, notes } = response.data
            if(status === "success"){
                if(notes.length > 0){
                    dispatch(filterNotesByStatus(notes));
                }
            }
        })
        .catch(error => dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        }))
}

export const filterNotSaveStatus = () => dispatch => {
    axios.get(`/api/not_save_status`, { headers: { 'Content-Type': 'application/json' } })
        .then(response => {
            console.log('filterNotSaveStatus', response.data);
            const { status, notes } = response.data
            if (status === "success") {
                dispatch(filterNotesByStatus(notes));
            }
        })
        .catch(error => dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        }))
}

export const getNotes = (notes) =>{
    return {
        type:GET_NOTES,
        payload:notes
    }
}

export const setNote = (note) =>{
    return {
        type: EDIT_NOTE,
        payload: note
    }
}

export const filterNotesByStatus = (notes) => {
    return {
        type: FILTER_NOTES,
        payload:notes
    }
}