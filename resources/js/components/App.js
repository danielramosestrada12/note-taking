import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CreateNoteTaking from './CreateNoteTaking';
import EditNoteTaking from './EditNoteTaking';
import NoteTaking from './NoteTaking';

const App = () => {
    return (
        <>
            <Router>
                <Switch>
                    <Route exact path="/" component={NoteTaking} />
                    <Route exact path="/create" component={CreateNoteTaking} />
                    <Route exact path="/edit" component={EditNoteTaking} />
                </Switch>
            </Router>
        </>
    );
};

export default App;