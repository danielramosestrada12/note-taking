import classnames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { addNotes } from './redux/action/noteAction';

class CreateNoteTaking extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
             title:"",
             text:"",
             status:"0",
             errors:{}
        }
    }

    onChangeRadioButton = e =>{
        this.setState({[e.target.name]:e.target.value});
    }

    // Handle OnChange Event
    onChange = e =>{
        this.setState({ [e.target.name]: e.target.value});
    }

    // Handle Submit Event
    onSubmit = e =>{
        console.log('Submitting...');
        e.preventDefault();
        // Destructure the state
        const {title, text, status} = this.state;
        // Store new note
        let newNote = {title, text, status};
        this.props.addNotes(newNote, this.props.history);
    }

    static getDerivedStateFromProps = (nextProps, nextState) =>{
        if(nextProps.errors){
            nextState.errors = nextProps.errors;
        }
        return null;
    }

    render = () => {
        // Destructure the state
        const {title, text, status, errors} = this.state;

        return (
            <div className="container">
                {/* Back to Note List */}
                <Link to="/" className="btn btn-outline-primary mt-2">
                    Go Back
                </Link>

                {/* Create Note Form */}
                <form onSubmit={this.onSubmit} className="mt-3">
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input type="text" className={classnames('form-control', {'is-invalid':errors.title})} id="title" name="title" 
                            value={title} placeholder="Enter Title" onChange={this.onChange}
                        />
                        {errors.title && <span className="text-danger">{errors.title}</span>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="text">Text</label>
                        <textarea type="text" className={classnames('form-control', { 'is-invalid': errors.text })} id="text" 
                            rows="3" cols="10" name="text" value={text} placeholder="Enter Text" 
                            onChange={this.onChange}
                        />
                        {errors.text && <span className="text-danger">{errors.text}</span>}
                    </div>
                    <div className={classnames('form-group form-inline', {'is-invalid':errors.status})} onChange={this.onChangeRadioButton} >
                        <div className="form-check mr-3">
                            <input className="form-check-input" type="radio" name="status" 
                                id="not_save" value="0" defaultChecked
                            />
                            <label className="form-check-label" htmlFor="not_save">
                                Not Save
                            </label>
                        </div>
                        <div className="form-check">
                            <input className="form-check-input" type="radio" name="status"
                                id="save" value="1" />
                            <label className="form-check-label" htmlFor="save">
                                Save
                            </label>
                        </div><br/>
                        {errors.status && <span className="text-danger">{errors.status}</span>}
                    </div>

                    {/* Submit Button */}
                    <button type="submit" className="btn btn-success">
                        Submit
                    </button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) =>{
    return{
        errors : state.errors
    }
} 

export default connect(mapStateToProps, {addNotes})(CreateNoteTaking);