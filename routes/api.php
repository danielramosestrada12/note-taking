<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::resource('note_taking', 'NoteTakingController')->except(['index', 'create', 'show']);
Route::get('/get_notes', 'NoteTakingController@get_notes');
Route::get('/get_both_status/{save}/{not_save}', 'NoteTakingController@save_not_save');
Route::get('/save_status', 'NoteTakingController@save_status');
Route::get('/not_save_status', 'NoteTakingController@not_save_status');
Route::get('/searchTitle/{search}', 'NoteTakingController@searchTitle');
