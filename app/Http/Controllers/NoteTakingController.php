<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteTakingRequest;
use App\NoteTaking;
use App\Repository\NoteTakingRepositoryInterface as NTRI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoteTakingController extends Controller
{
    protected $note_taking;

    public function __construct(NTRI $note_taking){
        $this->note_taking = $note_taking;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteTakingRequest $request)
    {
        $data = ['title' => $request->title, 'text' => $request->text, 'status' => $request->status];
        $this->note_taking->create($data);
        return response()->json(['status' => 'success', 'message' => 'New note successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = $this->note_taking->findOrFail($id);
        return response()->json(['status' => 'success', 'note' => $note]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NoteTakingRequest $request, $id)
    {
        $data = ['title' => $request->title, 'text' => $request->text, 'status' => $request->status];
        $this->note_taking->update($id, $data);
        return response()->json(['status' => 'success', 'message' => 'Note successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->note_taking->delete($id);
        return response()->json(['status' => 'success', 'message' => 'Note successfully deleted']);
    }

    public function get_notes(){
        $notes = $this->note_taking->get();
        $notes->count() > 0 ? $response = response()->json(['status' => 'success', 'notes' => $notes]) :
        $response = response()->json(['status' => 'failed', 'message' => 'No Notes Found.']);
        return $response;
    }

    public function save_status(){
        // Save
        $note_taking = $this->note_taking->get_save_status();
        return response()->json(['status' => 'success', 'notes' => $note_taking]);
    }

    public function not_save_status(){
        // Not Save
        $note_taking = $this->note_taking->get_not_save_status();
        return response()->json(['status' => 'success', 'notes' => $note_taking]);
    }

    public function searchTitle($search){
        $note_taking = $this->note_taking->search($search);
        $note_taking->count() > 0 ? $response = response()->json(['status' => 'success', 'notes' => $note_taking]) :
        $response = response()->json(['status' => 'failed', 'message' => 'No Notes Found.']);
        return $response;
    }
}
