<?php

namespace App\Repository;

interface NoteTakingRepositoryInterface{
    // Get All Notes
    public function get($order = "id", $sort = "ASC");

    // Create Note
    public function create(array $data);

    // Search Notes
    public function search($search, $order = "id", $sort = "ASC");

    // Get 1 Note
    public function findOrFail($value);

    // Update Note
    public function update(int $id, array $data);

    // Delete Note
    public function delete(int $id);

    public function get_save_status($order = "id", $sort = "ASC");

    public function get_not_save_status($order = "id", $sort = "ASC");
}