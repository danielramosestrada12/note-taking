<?php

namespace App\Repository;

use App\NoteTaking;

class NoteTakingRepository implements NoteTakingRepositoryInterface{

    protected $note_taking;

    public function __construct(NoteTaking $note_taking){
        $this->note_taking = $note_taking;
    }

    // Get All Notes
    public function get($order = "id", $sort = "ASC"){
        return $this->note_taking->orderBy($order, $sort)->get();
    }

    // Create Note
    public function create(array $data){
        return $this->note_taking->create($data);
    }

    // Search Notes
    public function search($search, $order = "id", $sort = "ASC"){
        return $this->note_taking->where('title', '=', "%".$search."%")->orderBy($order, $sort)->get();
    }

    // Get 1 Note
    public function findOrFail($value){
        return $this->note_taking->findOrFail($value);
    }

    // Update Note
    public function update(int $id, array $data){
        $note_taking = $this->note_taking->findOrFail($id);
        return $note_taking->update($data);
    }

    // Delete Note
    public function delete(int $id){
        $note_taking = $this->note_taking->findOrFail($id);
        return $note_taking->delete();
    }

    public function get_save_status($order = "id", $sort = "ASC"){
        return $this->note_taking->where('status', 1)->orderBy($order, $sort)->get();
    }

    public function get_not_save_status($order = "id", $sort = "ASC"){
        return $this->note_taking->where('status', 0)->orderBy($order, $sort)->get();
    }
}