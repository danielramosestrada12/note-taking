<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteTaking extends Model
{
    protected $fillable = ['title', 'text', 'status'];
}
